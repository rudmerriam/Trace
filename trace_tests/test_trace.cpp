//==============================================================================
//  Copyright (c) 2024. Mystic Lake Software
//
//  This is free software; you can redistribute it and/or modify it under
//  the terms of the GNU General Public License  as published by the Free
//  Software Foundation; either version 3 of the License, or (at your
//  option) any later version.
//
//  This is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
//  for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http:www.gnu.org/licenses/>.
//==============================================================================
#include <tut/tut.hpp>
#include <tut/tut_reporter.hpp>

#include "Trace.h"

//--------------------------------------------------------------------------------------------------------------------------
auto main() -> int {
   using runner = tut::test_runner_singleton;
   tut::reporter reporter;
   runner::get().set_callback(&reporter);

   tut::test_result tr;

   mys::TraceOff t_out(mys::tout);
   mys::TraceOff t_tmp(mys::ttmp);

   runner::get().run_tests("Trace Output POD");
   runner::get().run_tests("Trace Output Containers");
   runner::get().run_tests("Trace Iomanip");

   return !reporter.all_ok();
}
