cmake_minimum_required(VERSION 3.27)
project(trace_test)

include(FetchContent)
FetchContent_Declare(
        TUT
        GIT_REPOSITORY https://github.com/mrzechonek/tut-framework.git
#        GIT_TAG master
        CONFIGURE_COMMAND ""
        BUILD_COMMAND ""
)
FetchContent_MakeAvailable(tut)
message("tut_SOURCE_DIR \t" ${tut_SOURCE_DIR})

include_directories(${tut_SOURCE_DIR}/include)
include_directories(../inc)

add_executable(${PROJECT_NAME} test_trace.cpp
               test_containers.cpp
               test_iomanip.cpp
               test_pod.cpp
               test_trace.cpp
)