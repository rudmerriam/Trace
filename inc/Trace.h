#pragma once
//==============================================================================
//  Copyright (c) 2024. Mystic Lake Software
//
//  This is free software; you can redistribute it and/or modify it under
//  the terms of the GNU General Public License  as published by the Free
//  Software Foundation; either version 3 of the License, or (at your
//  option) any later version.
//
//  This is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
//  for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http:www.gnu.org/licenses/>.
//==============================================================================
#include <algorithm>
#include <chrono>
#include <iomanip>
#include <iostream>
#include <ranges>
#include <sstream>
//---------------------------------------------------------------------------------------------------------------------
namespace mys {

#include "TraceBase.h"
#include "TraceOnOff.h"

   //---------------------------------------------------------------------------------------------------------------------
#ifndef mys_chars
#define mys_chars
   constexpr auto tab {'\t'};
   constexpr auto nl {'\n'};
   constexpr auto sp {' '};
   constexpr auto comma {", "};
#endif
   //---------------------------------------------------------------------------------------------------------------------
   class Trace : public TraceBase {
   public:
      Trace(std::ostream& os, char const ch) : TraceBase {os, ch} {
         os << std::unitbuf;
      }
      Trace(TraceBase const& other) = delete;
      Trace(TraceBase&& other) = delete;

      Trace& operator=(TraceBase const& other) final = delete;

      template<typename T> TraceBase& operator<<(T const& value);

   private:
      void time_stamp();
   };
   //---------------------------------------------------------------------------------------------------------------------
   template<typename T> inline TraceBase& Trace::operator<<(T const& value) {
      if (mEnabled) {
         mHex = mOs.flags() & std::ios_base::hex;
         mWidth = mOs.width();

         mOs << nl;// << std::flush;

         emit(mLeadChar);
         emit(',');
         time_stamp();
         emit(value);
         //        mOs << std::flush;
      }
      return mTrace;
   }
   //---------------------------------------------------------------------------------------------------------------------
   inline void Trace::time_stamp() {
      if (mEnabled) {
         using namespace ::std::chrono;
         time_point now = system_clock::now();
         std::time_t timer = system_clock::to_time_t(now);

         mOs << std::dec;
         emit(std::put_time(std::localtime(&timer), "%y%m%d,%H%M%S,"));
         mOs << std::setfill('0') << std::dec << std::setw(3);
         mOs << (duration_cast<milliseconds>(now.time_since_epoch()) % 1000).count();
         emit(": ");
      }
   }
   /*=====================================================================================================================
    * Outputs convert std::cout which control what TraceBase statements are output
    *  All outputs are on by default but can be selectively turned off globally
    *  and enabled locally... or turned on globally and off locally
    *
    *      mys::TraceBaseOff dbg_ctrl { mys::tdbg };    // turn off tdbg (debug)
    *      mys::TraceBaseOn dbg_ctrl { mys::tdbg };     // turn of tdbg (debug)
    *
    *  These are object creations so their destructors are called when the containing
    *  block is left which reverses their action. This is regardless of the global
    *  setting, i.e. global off will be reversed when TraceBaseOff leaves a block.
    *
    *  TODO: look at global control.
    *
    */

   inline Trace tlog {::std::cout, 'L'};
   inline Trace tout {::std::cout, 'O'};
   inline Trace tdbg {::std::cout, 'D'};
   // for temporary use. different so easily found for removal
   inline Trace ttmp {::std::cout, 'T'};

   /*=====================================================================================================================
    *  Some utility defines. Unfortunately through C++17 can't get function names, etc
    *
    *  code_line: display function and file line number TraceBase statements
    *  code_enty: display function and 'entry'
    *  code_exit: display function and 'exit' for return at end of function
    *  code_return: display function and 'return' for an early return from function
    */

}    // namespace

#define code_line std::setw(20) << __FUNCTION__ << mys::sp << std::dec << __LINE__ << mys::tab
#define code_entry code_line << "entry" << mys::sp
#define code_exit code_line << "exit" << mys::sp
#define code_return code_line << "return" << mys::sp

#define code_line_text(text) std::setw(20) << (text) << mys::sp << std::dec << __LINE__ << mys::tab
#define code_entry_named(text) code_line_text(text) << "entry" << mys::sp
