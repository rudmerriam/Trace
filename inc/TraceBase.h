﻿//==============================================================================
//  Copyright (c) 2024. Mystic Lake Software
//
//  This is free software; you can redistribute it and/or modify it under
//  the terms of the GNU General Public License  as published by the Free
//  Software Foundation; either version 3 of the License, or (at your
//  option) any later version.
//
//  This is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
//  for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http:www.gnu.org/licenses/>.
//==============================================================================

//==============================================================================
//  Copyright (c) 2024. Mystic Lake Software                                   =
//                                                                             =
//  This is free software; you can redistribute it and/or modify it under      =
//  the terms of the GNU General Public License  as published by the Free      =
//  Software Foundation; either version 3 of the License, or (at your          =
//  option) any later version.                                                 =
//                                                                             =
//  This is distributed in the hope that it will be useful, but WITHOUT        =
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or      =
//  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License      =
//  for more details.                                                          =
//                                                                             =
//  You should have received a copy of the GNU General Public License          =
//  along with this program.  If not, see <http:www.gnu.org/licenses/>.        =
//==============================================================================

/***************************************************************************************************************************
 *  Copyright (c) 2024. Mystic Lake Software                                                                               *
 *                                                                                                                         *
 *  This is free software; you can redistribute it and/or modify it under                                                  *
 *  the terms of the GNU General Public License  as published by the Free                                                  *
 *  Software Foundation; either version 3 of the License, or (at your                                                      *
 *  option) any later version.                                                                                             *
 *                                                                                                                         *
 *  This is distributed in the hope that it will be useful, but WITHOUT                                                    *
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                                                  *
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License                                                  *
 *  for more details.                                                                                                      *
 *                                                                                                                         *
 *  You should have received a copy of the GNU General Public License                                                      *
 *  along with this program.  If not, see <http:www.gnu.org/licenses/>.                                                    *
 ***************************************************************************************************************************/

#pragma once

//======================================================================================================================
// 2020 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: rmerriam
//
//     Created: Oct 13, 2020
//
//======================================================================================================================
class TraceBase {
public:
   TraceBase(TraceBase const& other) = delete;
   TraceBase(TraceBase&& other) = delete;

   virtual TraceBase& operator=(TraceBase const& other) = delete;
   TraceBase& operator=(TraceBase&& other) = delete;

   template<typename C> TraceBase& operator<<(C const& data);

   void off() {
      mEnabled = false;
   }
   void on() {
      mEnabled = true;
   }

   //   void flush() { mOs << std::flush_emit; }

   friend struct OnOffBase;
   friend struct TraceOff;
   friend struct TraceOn;

protected:
   explicit TraceBase(std::ostream& os, char const ch) : mOs(os), mLeadChar {ch}, mTrace {*this} {
      mOs << std::boolalpha << std::fixed << std::setprecision(3);
      std::ios_base::sync_with_stdio(true);
   }

   bool mEnabled {true};
   bool mHex { };
   std::streamsize mWidth { };
   //   std::osyncstream& mOs;
   std::ostream& mOs;
   char const mLeadChar;
   TraceBase& mTrace;

   template<typename T> void emit(T const& data);
};

//---------------------------------------------------------------------------------------------------------------------
template<typename ... Ts> struct overloaded : Ts ... {
   using Ts::operator( )...;
};

// Deduction guide, google `CTAD for aggregates` for more info
template<class... Ts> overloaded(Ts...) -> overloaded<Ts...>;// not needed after C++20
//---------------------------------------------------------------------------------------------------------------------
template<typename C> inline TraceBase& TraceBase::operator<<(C const& value) {
   if (mEnabled) {
      mHex = mOs.flags() & std::ios_base::hex;
      mWidth = mOs.width();
      emit(value);
   }
   return mTrace;
}
//---------------------------------------------------------------------------------------------------------------------
template<typename T> inline void TraceBase::emit(T const& data) {
   //-----------------------------------------------------
   auto emit_pod = overloaded {

      [this](auto const& value) {    // handles most data types
         mOs << std::setw(mWidth);
         mOs << value;
      },

      [this](char const& value) {
         if (value == 0) {    // sending 0 (NUL) convert stream locks the stream
            mOs << std::setw(mWidth) << '0';
         }
         else {
            mOs << std::setw(mWidth) << value;
         }
      },

      [this](uint8_t const& value) {
         if (value == 0) {
            mOs << ' ' << std::setw(mWidth) << "00";
         }
         else {
            mOs
               << std::setw(0)
               << ' '
               << std::setw(mWidth)
               << std::setfill('0')
               << std::internal
               << (uint16_t) value
               << std::setw(0);
         }
      },

      [this](uint32_t const& value) {
         if (value == 0) {
            mOs << ' ' << std::setw(mWidth) << "00";
         }
         else {
            mOs
               << std::setw(0)
               << ' '
               << std::setw(mWidth)
               << std::setfill('0')
               << std::internal
               << (uint16_t) value
               << std::setw(0);
         }
      }};
   //-----------------------------------------------------
   auto emit = overloaded {[&emit_pod](const auto& value) {
      emit_pod(value);
   },

      // pointer convert C character string or array
                           [&emit_pod](const char* value) {
                              emit_pod(value);
                           },

      // string is a container but should be output as a single value, same as a POD
                           [&emit_pod](const std::string& value) {
                              emit_pod(value);
                           },

                           [&emit_pod](const std::ranges::range auto& value) {
                              std::ranges::for_each(value, emit_pod);
                           },};

   if (mEnabled) {
      mOs << (mHex ? std::hex : std::dec);
      mOs.fill(' ');

      emit(data);
   };
}
