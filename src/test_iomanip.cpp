//==============================================================================
//  Copyright (c) 2024. Mystic Lake Software
//
//  This is free software; you can redistribute it and/or modify it under
//  the terms of the GNU General Public License  as published by the Free
//  Software Foundation; either version 3 of the License, or (at your
//  option) any later version.
//
//  This is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
//  for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http:www.gnu.org/licenses/>.
//==============================================================================
#include <tut/tut.hpp>

#include "Trace.h"

using ustring = std::basic_string<uint8_t>;
//--------------------------------------------------------------------------------------------------------------------------
struct trace_test_iomanip {
   std::stringstream str_out;

   std::osyncstream sync_out {str_out};

   mys::Trace tstrm {sync_out, 'S'};

   //--------------------------------------------------------------------------------------------------------------------------
   std::string test_output() {
      tstrm.flush();
      sync_out.emit();
      return str_out.rdbuf()->str().substr(22);
   }
};
//=====================================================================================================================
namespace tut {

   typedef test_group <trace_test_iomanip> tg;
   tg io_name("Trace Iomanip");

   typedef tg::object trace_test;
   //--------------------------------------------------------------------------------------------------------------------------
   //  setw
   template<>
   template<>
   void trace_test::test<1>() {
      std::string t_name("std::setw");
      set_test_name(t_name);
      std::string td {" 12  456"};

      tstrm << std::setw(3) << 12 << std::setw(5) << 456;
      mys::ttmp << code_line << test_output() << mys::nl;
      ensure_equals(t_name + " failed", test_output(), td);
   }
   //--------------------------------------------------------------------------------------------------------------------------
   //  hex / dec
   template<>
   template<>
   void trace_test::test<2>() {
      std::string t_name("std::hex / std::dec");
      set_test_name(t_name);
      std::string td {"   c  456"};

      tstrm << std::hex << std::setw(4) << 12 << std::setw(5) << std::dec << 456;
      mys::ttmp << code_line << test_output() << mys::nl;
      ensure_equals(t_name + " failed", test_output(), td);
   }
   //--------------------------------------------------------------------------------------------------------------------------
   //  dec reset after hex
   template<>
   template<>
   void trace_test::test<3>() {
      std::string t_name("std::dec reset after std::hex ");
      set_test_name(t_name);
      std::string td {"   c  1c8    c  456"};

      tstrm << std::hex << std::setw(4) << 12 << std::setw(5) << 456;
      tstrm << std::hex << std::setw(4) << 12 << std::setw(5) << std::dec << 456;

      mys::ttmp << code_line << test_output() << mys::nl;
      std::string output = test_output().substr(0, 9) + test_output().substr(9 + 21);
      ensure_equals(t_name + " failed", output, td);
   }
}
